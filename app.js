var express = require('express');
var path = require('path');
var app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');



app.use(cors());

// Settings
app.set('port',80)

//Resize programation
//Here is solved the problem of the images resizing. the code check every so often if theres a new image and convert it, this makes in
//the background
//I cann't implemented because there is a problem of compatibility with Heroku.
// var j = schedule.scheduleJob('*/1 * * * *', function(){
//     fs.readdir('./uploads',(err,files)=>{
//         console.log(files)
//         for(let file of files){
//             if (!fs.existsSync(`./optimized/${file}`)) {
//                 fs.readFile(`./uploads/${file}`,null,(err,buffer)=>{
//                     sharp(buffer)
//                         .resize(120)
//                         .toFile(`./optimized/${file}`,(err, info) => { console.log(info) });
//                 })
//
//             }
//         }
//
//     })
// });


//Middlewares
//app.use(bodyParser.urlencoded({extended: false, limit: "10mb"}));
app.use(bodyParser.json({limit: '10mb', extended: true}));


//Routes
app.use(require('./routes/index'));

//Starting Server
app.listen(app.get('port'),()=>{
    console.log(`Running Server on port ${app.get('port')}`);
})

//Database
mongoose.connect('mongodb://carlos:carlos223@ds157136.mlab.com:57136/ecusproject', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex:true }, (err,res) => {
    if(err) throw err;
    console.log("DB connected");
});

//Statics
app.use("/",express.static(path.join(__dirname,'/public')));
app.use('/view',express.static(path.join(__dirname,'/public')));
app.use("/login",express.static(path.join(__dirname,'/public')));
app.use('/uploads', express.static('uploads'));



module.exports = app;

import {Component} from "react";
import React from "react";
import Nav from "./components/navigation-bar/Nav"
import Login  from "./components/login/Login";
import AppRoutes from "./routes";
import Viewer from "./components/photo-viewer/Viewer";
import {Route,Switch} from 'react-router-dom'

class App extends Component{
    render(){

        return (
            <div className="main">
                <Nav/>
                <Switch>
                    <Route exact path="/login" component={Viewer}/>
                    <Route exact path="/view" component={Viewer}/>
                    <Route exact path="/" component={Login}/>
                </Switch>

            </div>


        );
    };

}

export default App;
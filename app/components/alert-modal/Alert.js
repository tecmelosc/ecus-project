import {Button, Modal, ModalBody, ModalFooter} from "react-bootstrap";
import React ,{Component} from "react";


class Alert extends Component{
    constructor(props){
        super(props);
    }



    render(){

        let onHide = () =>{
            this.props.onHide();
        }

        return(
            <Modal show={this.props.modal_state} onHide={onHide} >
                <ModalBody>
                    <div className="row">
                        <div className="col-12 text-center" >
                          <h1>Alert</h1>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 text-center">
                            <h5>{this.props.modal_message}</h5>
                        </div>
                    </div>

                </ModalBody>

                <ModalFooter>
                    <Button onClick={onHide} variant="secondary">Close</Button>
                </ModalFooter>
            </Modal>
        )
    }
}


export default Alert;
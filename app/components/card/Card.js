import React, {Component} from 'react';
import './card.css'
import {CopyToClipboard} from 'react-copy-to-clipboard';
import copy from 'copy-to-clipboard';



class Card extends Component{

    token=localStorage.getItem('token')

    constructor(props){
        super(props);
    }

    render(){

        const editImage = () =>{
            this.props.onSelectEdit(this.props.imageData)
        }

        const deleteImage = () => {
            this.props.onSelectDelete(this.props.imageData._id)
        }

        const downloadImage = () => {
            fetch(`/image/download/${this.props.imageData._id}`,{
                method:"GET",
                headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":this.token
                }
            }).then(data=>{
                console.log(data)
            })
        }

        const shareLinkImage = () => {
            fetch(`/image/setpermission/${this.props.imageData._id}`,{
                method:"GET",
                headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":this.token
                }
            }).then(res=>res.json())
                .then(data=>{
                    copy(data.url);
                })
        }


        return(
            <div className="card">
                    <img src={this.props.imageData.server_uri} className="card-img-top" />
                <div className="card-body">
                        <h5 className="card-title">{this.props.imageData.name}</h5>
                        <p className="card-text">{this.props.imageData.description}</p>

                    <hr/>
                    <div className="section mr-3">
                        <div className="row">
                            <div className="col-6">
                                <button type="button" onClick={editImage} className="btn btn-block ml-3 btn-primary btn-sm "><i className="fas fa-edit"></i> Edit </button>
                            </div>
                            <div className="col-6">
                                <button onClick={deleteImage} type="button" className="btn btn-block btn-danger btn-sm"><i className="fas fa-trash"></i> Delete</button>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-6">
                                <button type="button" onClick={shareLinkImage} className="btn btn-block ml-3 btn-warning btn-sm "><i className="fas fa-copy"></i> Share</button>
                            </div>
                            <div className="col-6">
                                <button onClick={downloadImage} type="button" className="btn btn-block btn-success btn-sm">
                                    <i className="fas fa-download"></i> Download</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default Card;
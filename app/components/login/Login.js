import './login.css'
import React, {Component} from 'react';
import ReactDom from 'react-dom';
import Aler from '../alert-modal/Alert'


class Login extends Component{

    constructor(props) {
        super(props);
        this.state={
            user:"",
            password:"",
            modal_state:false,
            modal_message:""
        }

        this.onChangeInput = this.onChangeInput.bind(this);
        this.loginUser = this.loginUser.bind(this)
    }


    loginUser(e) {
        e.preventDefault()
        console.log(this.state)
        let body = this.state
        delete body.modal_state;
        delete body.modal_message;
        this.httpLogin(body)

        fetch('/user/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then(res => res.ok ? res.json():null)
        .then(data=>{
                localStorage.setItem('token',data.token);
                this.props.history.push('/view')
        }).catch(err=>{
            this.setState({
                modal_state:true,
                modal_message:"Invalid user or password"
            });        })
    }


    onChangeInput(e){
        const {name, value} = e.target;
        console.log(e.target.value)
        this.setState({
            [name]:value
        })
    }



    httpLogin(body){

    }

    render(){
        let onHide = () =>{
                this.setState({
                    modal_state:false,
                    modal_message:""
                });
        }
        return(
            <div className="login" >
                <div className="container-form-login animated fadeIn slow ">
                    <div className="row">
                        <div className="col-lg-4 offset-lg-4">
                            <img className=" user-image" src="assets/svg/user.svg"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 offset-lg-4">
                            <form className="form-login" onSubmit={this.loginUser}>
                                <div className="form-group">
                                    <label htmlFor="input1">User</label>
                                    <input name="user" type="text" onChange={this.onChangeInput} className="form-control" id="input1"
                                           aria-describedby="input1" placeholder="Enter user name"/>
                                    {/*<small  className="form-text text-muted">We'll never share your email with*/}
                                        {/*anyone else.*/}
                                    {/*</small>*/}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="input2">Password</label>
                                    <input name="password" onChange={this.onChangeInput} type="password" className="form-control" id="input2"
                                           placeholder="Password"/>
                                </div>
                                <button type="submit" className="btn btn-primary btn-block">Log in</button>
                            </form>
                        </div>
                    </div>

                </div>
                <Aler modal_state={this.state.modal_state} modal_message={this.state.modal_message} onHide={onHide} />
            </div>
        )
    }
}

export default Login


import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from "react-bootstrap";


class Delete extends Component{

    token=localStorage.getItem('token')

    constructor(props){
        super(props)
    }

    render(){

        const onHide = () =>{
            this.props.onHide()
        }

        const deleteImage = ()=> {
            fetch(`/image/${this.props.idDelete}`,{
                method:"DELETE",
                headers:{
                    "Authorization":this.token,
                    "Content-Type":"application/json",
                    "Accept":"application/json"
                }
            }).then(res=>res.json())
                .then(data=>{
                    this.props.onDelete();
                })
                .catch(err=>{console.log(err)})
        }

        return(
            <Modal show={this.props.modal_state} onHide={onHide}>
                <ModalBody>
                    <div className="row">
                        <div className="col-12 text-center" >
                            <h3>Delete Image Confirmation</h3>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 text-center">
                            <h5>Are you sure you want to delete the image?</h5>
                        </div>
                    </div>

                </ModalBody>

                <ModalFooter>
                    <Button onClick={deleteImage} variant="primary">Accept</Button>
                    <Button onClick={onHide} variant="danger">Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

export default Delete;
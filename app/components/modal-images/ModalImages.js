import React, {Component} from 'react';
import ReactDom from 'react-dom';
import Card from '../card/Card';
import Alert from '../alert-modal/Alert'
import './modal-images.css'
import {Modal, Button, ModalBody,ModalFooter,ModalTitle,ModalDialog} from 'react-bootstrap'
import FileBase64 from 'react-file-base64';



class ModalImages  extends Component{

    token=localStorage.getItem('token');

    constructor(props){
        super(props);
        this.state={
            show:props.modal_state,
            image_form:"",
            name_form:"",
            description_form:"",
            file:{base64:""},
            name:"",
            description:"",
            show_modal_alert:false,
            modal_alert_message:""
        }

        this.sendImage = this.sendImage.bind(this)
        this.onChangeInput = this.onChangeInput.bind(this)

    }

    getFiles(file){
        this.setState({
            file: file,
            image_form:file.base64})
        console.log(this.state.file)
    }

    putImage(e){
        // fetch("/image",{
        //     method:"POST",
        //     headers:{
        //         "Content-Type":"application/json",
        //         "Accept":"application/json",
        //         "Authorization":this.token
        //     },
        //     body:JSON.stringify({
        //         file:this.state.file,
        //         name:this.state.name,
        //         description:this.state.description,
        //     })
        // })
        //     .then(res => res.json() )
        //     .then(data => {console.log(data)
        //         this.props.onAdd()
        //     })
        //     .catch(err => {console.log(err)
        //         this.setState({
        //             show_modal_alert: true,
        //             modal_alert_message:"Image not loaded, try later"
        //         })
        //         this.props.onHide()
        //     })

        console.log(e.target.value)
    }

    postImage(){
        fetch("/image",{
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":this.token
            },
            body:JSON.stringify({
                file:this.state.file,
                name:this.state.name,
                description:this.state.description,
            })
        })
            .then(res => res.json() )
            .then(data => {console.log(data)
                this.props.onAdd()
            })
            .catch(err => {console.log(err)
                this.setState({
                    show_modal_alert: true,
                    modal_alert_message:"Image not loaded, try later"
                })
                this.props.onHide()
            })
    }

    sendImage(e){
        e.preventDefault()
        if(this.props.open_type=="new")
            this.postImage()
        else if(this.props.open_type=="edit")
            this.putImage(e)

    }

    onChangeInput(e){
        const {name, value} = e.target;
        this.setState({
            [name]:value
        })
        console.log(this.state.description)
    }




    render(){
        let title,button;

        let initModal = () => {
            console.log("onEditData",this.props.onEditData)
                this.setState({
                    image_form:this.props.onEditData.server_uri,
                    name_form:this.props.onEditData.name,
                    description_form:this.props.onEditData.description
                })

        }

        let modalClose =  ()=>{
            this.setState({
                show_modal_alert:false,
                modal_alert_message:""

            })
        }

        let onHide = () =>{
            this.props.onHide();
            console.log(this.props.open_type)
        }
        if(this.props.open_type=="edit"){
            title="Edit Image";
            button="Save"
        }else if(this.props.open_type=="new"){
            title="Add a Image";
            button="Upload Image"
        }else{
            title=null;
            button=null
        }
        return(

            <div>


                <Modal show={this.props.modal_state} onHide={onHide} onShow={initModal}>
                    <div className="modal-header" >
                            <h4>{title}</h4>
                        <button type="button-" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>
                    <ModalBody>
                        <div className="row mb-5">
                            <div className="col-10 offset-1">
                                <form onSubmit={this.sendImage}>
                                    <div className="form-group">
                                        <label htmlFor="input1">Name</label>
                                        <input onChange={this.onChangeInput} name="name" defaultValue={this.state.name_form}  type="text" className="form-control"  id="input1"
                                               placeholder="Enter a image name"/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="textarea1">Description</label>
                                        <textarea onChangeCapture={this.onChangeInput} defaultValue={this.state.description_form} name="description" className="form-control" id="textarea1" rows="3"></textarea>
                                    </div>
                                    <FileBase64
                                        multiple={ false }
                                        onDone={ this.getFiles.bind(this) } />

                                    <img className="image-modal" src={this.state.image_form} alt=""/>

                                    <button type="submit" className="btn btn-primary mt-4 btn-block">{button}</button>

                                </form>
                            </div>
                        </div>



                    </ModalBody>
                </Modal>


            </div>

        )
    }
}


export default ModalImages;
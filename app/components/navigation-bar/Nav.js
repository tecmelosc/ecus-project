import './nav.css'
import React, {Component} from 'react';
import ReactDom from 'react-dom';

class Nav extends Component{
    render(){
        return(
            <nav className="navbar navbar-light justify-content-center">
                <a className="navbar-brand" href="#">
                    <img src="assets/svg/ecus-logo.svg" width="30" height="30"
                         className="d-inline-block align-top" alt="" />
                    <span className="ml-2">Ecus App</span>
                </a>
            </nav>
        )
    }

}

export default Nav;
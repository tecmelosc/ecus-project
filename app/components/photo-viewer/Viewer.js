import React, {Component} from 'react';
import Card from '../card/Card';
import './viewer.css'
import ModalImages from "../modal-images/ModalImages";
import Delete from '../modal-delete/Delete'
import Alert from "../alert-modal/Alert";

class Viewer extends Component{

    token=localStorage.getItem('token');

    constructor(props){
        super(props);
        this.state={
            show_image_modal:false,
            open_type:"",
            images:[],
            show_modal_alert: false,
            modal_alert_message:"",
            show_modal_delete: false,
            selected_delete:"",
            selected_edit:{},
        }
    }



    componentDidMount() {
       this.updateList();
    }



    updateList(){
        fetch('/image',{
            method:"GET",
            headers:{
                "Authorization":this.token,
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }).then(res=>res.json())
            .then(data=>{
                console.log(data)
                this.setState({
                    images:data,
                    open_type:""
                })
            }).catch(err=>{
                this.setState({
                    images:[]
                })
            }
        )
    }

    render(){

        let onHideImages =() =>{
            this.setState({
                show_image_modal:false,
                open_type:null,
            });


        }

        let showNewImageModal =() => this.setState({
            show_image_modal:true,
            open_type:"new"
        })

        let onEdit = (data_edit)=> {
            this.state.setState({
                show_image_modal:true,
                open_type:"edit"
            })
        }

        let onDelete = (id) =>{
            this.setState({
                show_modal_delete:false,
                selected_delete:id,
                modal_alert_message:"File deleted successfully",
                show_modal_alert:true

            })
            this.updateList()
        }

        let onSelectDelete = (id)=>{
            this.setState({
                show_modal_delete:true,
                selected_delete:id
            })
        }

        let onSelectEdit = (dataImage) => {
            this.setState({
                open_type:"edit",
                selected_edit:dataImage
            },err=>{
                this.setState({
                    show_image_modal:true
                })
            })
        }

        let onHideDelete = () => {
            this.setState({
                show_modal_delete:false
            })
        }

        let onAdd = () =>{
            this.setState({
                show_image_modal:false,
                show_modal_alert:true,
                modal_alert_message:"Image uploaded succesfully"
            })

            this.updateList()
        }

        let onHideAlert = ()=>{
            this.setState({
                show_modal_alert:false
            })
        }


         return (
            <div className="container">
                <div className="title-gallery row">
                    <div className="col-12 text-center">
                        <h1>Your Gallery</h1>
                    </div>
                </div>
                <div className="row">
                    <hr className="divider"/>
                </div>
                <div className="row mt-3 mb-4">
                    <div className="col-8 offset-2">
                        <button onClick={showNewImageModal} className="btn button-add btn-block btn-primary"><i className="fas fa-plus"></i> New Image</button>
                    </div>
                </div>
                <div className="row mt-4">
                    {this.state.images.map((image)=>{
                        return(
                            <div className="col-4 mb-3 animate flipInX" key={image._id}>
                                <Card imageData={image} onSelectDelete={onSelectDelete} onSelectEdit={onSelectEdit}/>
                            </div>
                        )
                    })}
                </div>
                <ModalImages modal_state={this.state.show_image_modal} onEditData={this.state.selected_edit} open_type={this.state.open_type} onHide={onHideImages} onEdit={onEdit} onAdd={onAdd} />
                <Delete modal_state={this.state.show_modal_delete} onHide={onHideDelete} onDelete={onDelete} idDelete={this.state.selected_delete} />
                <Alert modal_state={this.state.show_modal_alert} modal_message={this.state.modal_alert_message} onHide={onHideAlert}/>

            </div>
        )
    }
}

export default Viewer;
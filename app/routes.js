import React from 'react'
import {Route,Switch} from 'react-router-dom'


import Viewer from "./components/photo-viewer/Viewer"
import Login from "./components/login/Login"
import App from "./App";

const AppRoutes = () => {
        <Switch>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/view" component={Viewer}/>
            <Route exact path="/" component={Login}/>
        </Switch>
}

export default AppRoutes;
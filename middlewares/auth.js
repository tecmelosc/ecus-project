const jwt = require('jsonwebtoken');

let verifyToken = (req,res,next) => {
    let token = req.get('Authorization');
    console.log("Token",token)
    jwt.verify (token, "Secrect Key Carlos Melo", (err, decoded) => {
        console.log(err)
        if(err!=null){
            return res.status(401).json({
                Authentication:"Failed"
            });
        }else{
            req.id_user=decoded.user_found._id;
        }
        next();
    });
};

let verifyImageToken = (req, res,next) => {
    let token = req.params.token;
    console.log(token)
    jwt.verify (token, "Secrect Key Carlos Melo", (err, decoded) => {
        console.log(err)
        if(err!=null){
            return res.send("You're not allowed to be here")
        }else{
            req.image_name=decoded.image_name;
        }
        next();
    });
}

module.exports = {
    verifyToken,
    verifyImageToken
}

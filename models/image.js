const mongoose= require('mongoose');
let Schema = mongoose.Schema;


let image_schema = new Schema(
    {
        id: {type:Schema.ObjectId},
        id_user: {
            type:Schema.ObjectId,
            ref:'User',
            require:'true'
        },
        name: {
            type:String
        },
        server_uri:{
            type:String
        },
        description: {
            type:String
        }
    }
)


module.exports = mongoose.model('Image', image_schema);

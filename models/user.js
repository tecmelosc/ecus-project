const mongoose= require('mongoose');
let Schema = mongoose.Schema;


let user_schema = new Schema(
    {
        id: {type:Schema.ObjectId},
        user: {type:String,
            required: [true, 'The user name is required'],
            unique:"true",
            sparse:"true"},
        password: {type:String,
            required: [true, 'The password is required']}
    }
)


module.exports = mongoose.model('User', user_schema);

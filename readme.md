# Ecus Technical Examn

This project is a images administrator, it's developed in JavaScript using two of its framework that are React for the Front End and NodeJS for
the Back End. The data base is built in MongoDB a no SQL database admin.


Project url:
http://68.183.212.117/

## Installation

Install all the dependencies of the project.

```bash
npm install
```
Run in a command line the node server

```bash
npm run start
```

For run in a  dev environment use for watch every change and recompile automa

```bash
npm run dev
```

You should run the webpack server for React´´

```bash
npm run webpack
```


For testing ´´

```bash
npm run test
```

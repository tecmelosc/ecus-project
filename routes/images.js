const model = require("../models/image");
var express = require('express');
const app = express();
const {verifyToken,verifyImageToken} = require('../middlewares/auth')
var fs = require("fs");
const jwt = require('jsonwebtoken');
const path = require('path')

app.post('/',verifyToken, (req, res) => {
    let file = req.body.file
    let file_parts=file.name.split(".");
    let file_extension = file_parts[1];
    let id_user=req.id_user;
    let full_uri = req.protocol + '://' + req.get('host');
    let base64Image=req.body.file.base64.replace(/^data:image\/\w+;base64,/, "");

    let image = new model({
            name:req.body.name,
            description:req.body.description,
            id_user:id_user
        });

    image.save((err,imageDB) => {
        if(err!=null)
            return res.status(400).json({
                message: "Image not stored in data base",
                error: err
            })

        let file_name=`./uploads/${imageDB._id}.${file_extension}`

        fs.writeFile(file_name, base64Image, 'base64', function(err) {
            if(err!=null)
                res.status(400).json({
                    message:"Image not stored in data"
                })
        });

        file_name=file_name.substr(1)
        model.findOneAndUpdate({_id:imageDB._id},{$set:{server_uri:`${full_uri}${file_name}`}},{new:true},(err,doc)=>{
            if(err!=null)
                return res.status(500).json({
                    message:"File not stores, try later"
                });

            res.status(200).json({
                message:"File stored succesfully"
            })
        })

    })
});


//This route hasn't implemented in the front end bur is tested in postman
//This route edit the images with its metadata
app.put('/:id',(req,res)=>{
    let id_user = req.id_user;
    let id = req.params.id;

        let file = req.body.file;
        let file_parts=file.name.split(".");
        let file_extension = file_parts[1];
        let base64Image=req.body.file.base64.replace(/^data:image\/\w+;base64,/, "");

    let image_updated={
        name:req.body.name,
        description:req.body.description
    }

    model.findOneAndUpdate({_id: id, id_user:id_user},image_updated,{new: true}, (err, doc) => {
        if (err != null)
            return res.status(500).json({
                ok: false,
                err
            });

        let file_name=`./uploads/${image_updated._id}.${file_extension}`
        fs.writeFile(file_name, base64Image, 'base64', function(err) {
            if(err!=null)
                res.status(400).json({
                    message:"Image not stored in data"
                })
        });

        res.status(200).json({
            message: "File edited succesfully"
        })

    })});



app.get('/',verifyToken,(req,res)=>{
    model.find({id_user:req.id_user},(err,docs)=>{
        if(err!=null)
            res.status(400).json({
                message:"Bad request, try later"
            })

        console.log(docs)

        res.status(200).json(
            docs
        )
    })
});

app.delete('/:id',verifyToken,(req, res)=>{
    var id=req.params.id;
    var uri="";
    var parts="";
    var image_name=""


    model.findOneAndRemove({_id:id},(err,doc)=>{
        if(err!=null)
            return res.status(500).json({
                message:"File not removed1"
            });
        uri=doc.server_uri
        parts=uri.split('/uploads/');
        image_name=parts[1]

        fs.unlink(`./uploads/${image_name}`,(err)=>{
            if(err!=null)
                return res.status(500).json({
                    message:"File not removed1"
                });
        })

        res.status(200).json({
            message:"File deleted successfully"
        })

    })
    var path=`./uploads/${id}`
})

app.get('/getpermission/:token',verifyImageToken ,(req, res, next) => {
    let token = req.params.token
    let image_name = req.image_name;
    console.log(image_name)

    let file=fs.readFile(`uploads/${image_name}`,(err,image)=>{
        if(err)
            res.status(400).send("Image not available")

        res.writeHead(200,{'Content-type':'image/jpg'});
        res.end(image);
    });
});

app.get('/download/:id',verifyToken,(req,res)=>{
    let id_image = req.params.id;

    model.findOne({_id:id_image},(err,doc)=>{
        if(err!=null)
            res.status(400).json({
                message:"File didn't found"
            })

        let uri=doc.server_uri;
        let parts=uri.split('uploads/');
        let image_name=parts[1];
        res.download("/Users/maccarlos/Documents/ecus/uploads/5cde71da1fc199b86daed874.jpg");
    })
})


app.get('/setpermission/:id',verifyToken,(req,res)=>{
    let id_image = req.params.id;
    let full_uri = req.protocol + '://' + req.get('host');
    model.findOne({_id:id_image},(err,doc)=>{
        if(err!=null)
            res.status(400).json({
                message:"File didn't found"
            })
        let uri=doc.server_uri
        let parts=uri.split('uploads/');
        let image_name=parts[1];

        let token=jwt.sign({
            image_name:image_name
        },"Secrect Key Carlos Melo", {expiresIn:10 * 6000});
        //10 minutes for a valid token to see the image


        res.status(200).json({
            url:`${full_uri}/image/getpermission/${token}`
        })
    })
})

module.exports = app;
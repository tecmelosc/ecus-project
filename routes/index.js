var express = require('express');
const app = express();

const users = require("./users");
const images = require("./images");

app.use("/user",users);
app.use("/image",images);

module.exports = app;

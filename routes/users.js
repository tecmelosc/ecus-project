var express = require('express');
const model = require("../models/user");
const app = express();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

//This route is not implemented in the front end, it's a user registration, when the user finish the register, the server assign
// a token to the user and it's saved in the local storage.
//Only the front part is implemented but itsn't connected.
app.post('/', (req, res) => {
    let unique_user=req.body.user
    console.log(req.body)
    model.findOne({user:unique_user},(err,user_finded)=>{
        if(user_finded)
            return res.status(400).json({
                message: "The user name is already registered",
            })
    });
    let user = new model({
        user:req.body.user,
        password:bcrypt.hashSync(req.body.password, 10)
    });

    user.save((err,userDB) => {
        if(err!=null)
            return res.status(400).json({
                state: "failed",
                message: "User not stored in the data base",
                error: err
            })
        delete userDB.password;
        let token = jwt.sign({userDB},"Secrect Key Carlos Melo", {expiresIn:"4h"});

        res.status(200).json({
            token: token,
            message: 'User registered succesfully'
        });

    })
});

app.post('/login',(req,res)=>{
    console.log(req.body);
    let user = {
        user:req.body.user,
        password: req.body.password
    }

    model.findOne({user:user.user},(err,user_found)=>{
        if(user_found){
            if(bcrypt.compareSync(user.password,user_found.password)){
                console.log("hooola")
                let token = jwt.sign({user_found},"Secrect Key Carlos Melo", {expiresIn:"1h"});
                return res.json({
                    token:token
                })
            }else{
                return res.status(400).json({
                    message: "Incorrect Password",
                })
            }
        }else{
            res.status(400).json({
                message: "User not found",
            })
        }

    })
})

module.exports = app;

process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let User = require('../models/user');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app.js');
let should = chai.should();
let model_user = require('../models/user')


chai.use(chaiHttp);

describe('/Post Token User', ()=>{
    it('It should give a valid token', (done)=>{
        let user={
            user:"carlos123",
            password:"carlos123"
        }

        chai.request(server)
            .post('/user')
            .send(user)
            .end((err,res) => {
                res.should.have.status(200);
                res.body.user.should.be.a('object');
                res.body.should.have.property('token');
                done();
            })
    })
})



describe('/Post Login User', ()=>{
    it('It should give a valid token when the user sign in', (done)=>{

        let user={
            user:"carlos",
            password:"carlos123"
        }

        chai.request(server)
            .post('user/login')
            .send(user)
            .end((err,res) => {
                res.should.have.status(200);
                res.body.user.should.be.a('object');
                res.body.should.have.property('token');
                done();
            })
    })
})

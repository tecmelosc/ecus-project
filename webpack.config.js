module.exports = {
    entry:'./app/index.js',
    output:{
        path: __dirname + '/public',
        filename: 'bundle.js',
        publicPath:'/'
    },
    module:{
        rules:[
            {
                use:'babel-loader',
                test:/\.js$/,
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loaders: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader',
                options:{
                    publicPath: __dirname + '/public/assets',
                    outputaPath: __dirname + '/public/assets'
                }
            }
        ],
    }
}